from django.contrib import admin

from .models import Client, Message, Mailing_List


admin.site.register(Client)
admin.site.register(Message)
admin.site.register(Mailing_List)
