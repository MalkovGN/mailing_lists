from django.urls import path

from .views import ClientViewSet, MessageViewSet, MailingListViewSet


clients_list = ClientViewSet.as_view({
    'get': 'list',
    'post': 'create',
    'delete': 'destroy'
})

clients_detail = ClientViewSet.as_view({
    'get': 'list',
    'put': 'update',
    'delete': 'destroy'
})

messages_list = MessageViewSet.as_view({
    'get': 'list',
    'post': 'create',
    'delete': 'destroy'
})

messages_detail = MessageViewSet.as_view({
    'get': 'list',
    'put': 'update',
    'delete': 'destroy'
})

mailinglist_list = MailingListViewSet.as_view({
    'get': 'list',
    'post': 'create',
    'delete': 'destroy'
})

mailinglist_detail = MailingListViewSet.as_view({
    'get': 'list',
    'put': 'update',
    'delete': 'destroy'
})

urlpatterns = [
    path('clients/', clients_list),
    path('clients/<int:pk>/', clients_detail),
    path('messages/', messages_list),
    path('messages/<int:pk>', messages_detail),
    path('mailing_list/', mailinglist_list),
    path('mailing_list/<int:pk>', mailinglist_detail),
]
