from rest_framework import serializers

from .models import Client, Message, Mailing_List, Send_Status


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = ['id', 'phone_number', 'operators_code', 'tag', 'time_zone']


class MessageSerializer(serializers.ModelSerializer):
    # send_status = serializers.SlugRelatedField(slug_field='status_name', read_only=True)

    class Meta:
        model = Message
        fields = ['id', 'send_time', 'send_status', 'mailing_list_id', 'clients_id']


class MailingListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mailing_List
        fields = ['id', 'sending_time', 'message', 'clients_filter', 'end_sending_time']


class SendStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = Send_Status
        fields = ['id', 'status_name']
