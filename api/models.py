from django.db import models
from django.core.validators import RegexValidator


class Mailing_List(models.Model):
    sending_time = models.DateTimeField()
    message = models.TextField()
    clients_filter = models.ForeignKey('Client', on_delete=models.CASCADE, null=True)
    end_sending_time = models.DateTimeField()

    def __index__(self):
        return self.id

    def __str__(self):
        return str(self.sending_time)


class Client(models.Model):
    phone_validate = RegexValidator(regex=r'^7\d{10}$')
    phone_number = models.CharField(max_length=11, validators=[phone_validate], unique=True)
    operators_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=20)
    time_zone = models.CharField(max_length=20)

    def __str__(self):
        return str(self.id)


class Message(models.Model):
    TO_SEND = 'to send'
    SENT = 'sent'

    SEND_STATUS = [
        (TO_SEND, 'To send'),
        (SENT, 'Sent'),

    ]

    send_time = models.DateTimeField()
    send_status = models.CharField(max_length=20, choices=SEND_STATUS, null=True)
    mailing_list_id = models.ForeignKey('Mailing_List', related_name='Mailing_list_id', on_delete=models.CASCADE, null=True)
    clients_id = models.ForeignKey('Client', related_name='clients_id', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.send_time)


class Send_Status(models.Model):
    status_name = models.CharField(max_length=20)

    def __str__(self):
        return self.status_name
