import now as now
import requests
from celery.utils.log import get_task_logger
from notification_service.celery import app

from .models import Message, Client, Mailing_List

logger = get_task_logger(__name__)


@app.task(bind=True, retry_backoff=True)
def send_message(
        self,
        data,
        client_id,
        Mailing_List_id,
        url='https://probe.fbrq.cloud/v1/send',
        token='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2ODE4MTY3NDMsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkBHTV9OYW1lIn0.oVwzFnbuXs7VJoJFZFEGexxWTw7Y-oWogLcJ64kcgP4'
):
    mail = Mailing_List.objects.get(pk=Mailing_List_id)
    client = Client.objects.get(pk=client_id)

    if mail.sending_time <= now.time() <= mail.end_sending_time:
        header = {
            'Authorization': token,
            'Content-Type': 'application/json'
        }
        try:
            requests.post(url=url + str(data['id']), headers=header, json=data)
        except requests.exceptions.RequestException as exc:
            logger.error(f"{data['id']} is error")
            raise self.retry(exc=exc)
        else:
            logger.info(f"{data['id']} Send status: Sent")
            Message.object.filter(pk=data['id']).update(send_status='Sent')
    else:
        time = 24 - (int(now.time().strftime('%H:%M:%S')[:2]) -
                     int(mail.time_start.strftime('%H:%M:%S')[:2]))
        logger.info(f"Message id: {data['id']}, "
                    f"Incorrect time to send,"
                    f"Restart sending after {60 * 60 * time} seconds")
        return self.retry(countdown=60 * 60 * time)
