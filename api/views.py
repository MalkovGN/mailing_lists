from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import viewsets, generics
from rest_framework.decorators import action

from .models import Client, Message, Mailing_List
from .serializers import MessageSerializer, ClientSerializer, MailingListSerializer
from . import serializers


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = serializers.MessageSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = serializers.ClientSerializer


class MailingListViewSet(viewsets.ModelViewSet):
    queryset = Mailing_List.objects.all()
    serializer_class = serializers.MailingListSerializer

    @action(detail=True, methods=['get'])
    def mailing_list_stats(self, request, pk=None):
        mailing_lists = Mailing_List.objects.all()
        get_object_or_404(mailing_lists, pk=pk)
        filtered_messages = Message.objects.filter(Mailing_List_id=pk).all()
        message_serializer = MessageSerializer(filtered_messages, many=True)
        return Response(message_serializer.data)

    @action(detail=False, methods=['get'])
    def total_stats(self, requests):
        mailing_lists_count = Mailing_List.objects.count()
        mailing_id = Mailing_List.objects.values('id')
        data = {
            'Number of mailing lists': mailing_lists_count,
            'Number of sent messages': ''
        }
        result = {}

        for m in mailing_id:
            res = {'Total messages': 0, 'To send': 0, 'Sent': 0}
            mailing = Message.objects.filter(Mailing_List_id=m['id']).all()
            to_send_counter = mailing.filter(send_status='To send').count()
            sent_counter = mailing.filter(send_status='Sent')
            res['Total messages'] = len(mailing)
            res['To send'] = to_send_counter
            res['Sent'] = sent_counter
            result[m['id']] = res

        data['Number of sent messages'] = result
        return Response(data)
